

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import contentassist.DotContentAssistProcessorTest;
import partitionscanner.EditorSourcePartitionScannerTest;
import validator.DotValidationTests;
import validator.HTMLValidatonTests;

@RunWith(Suite.class)
@SuiteClasses({ DotValidationTests.class, 
                HTMLValidatonTests.class,
                EditorSourcePartitionScannerTest.class,
                DotContentAssistProcessorTest.class })
public class AllTests {

}
