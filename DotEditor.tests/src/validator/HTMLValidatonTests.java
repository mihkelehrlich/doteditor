package validator;

import testplugin.Activator;

import com.aptana.core.build.IBuildParticipant;
import com.aptana.editor.html.validator.HTMLParseErrorValidatorTest;
import com.aptana.editor.html.validator.HTMLParserValidator;

public class HTMLValidatonTests extends HTMLParseErrorValidatorTest {
    @Override
    protected IBuildParticipant createValidator() {
        return new HTMLParserValidator(){
            @Override
            protected String getPreferenceNode() {
                return Activator.PLUGIN_ID;
            }
        };
    }
}
