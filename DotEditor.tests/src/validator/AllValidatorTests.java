package validator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DotValidationTests.class, HTMLValidatonTests.class})
public class AllValidatorTests {

}
