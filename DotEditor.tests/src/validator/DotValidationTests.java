package validator;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.junit.Test;

import com.aptana.buildpath.core.tests.AbstractValidatorTestCase;
import com.aptana.core.build.IBuildParticipant;
import com.aptana.core.build.IProblem;
import com.aptana.core.build.IProblem.Severity;
import com.aptana.editor.html.core.IHTMLConstants;
import com.aptana.editor.html.parsing.HTMLParseState;
import com.aptana.editor.html.validator.HTMLParserValidator;
import com.aptana.js.core.IJSConstants;
import com.aptana.parsing.IParseState;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.*;
import editor.EditorSourceConfiguration;

public class DotValidationTests extends AbstractValidatorTestCase {

    @Override
    protected IBuildParticipant createValidator() {
        return new HTMLParserValidator() {
            @Override
            protected String getPreferenceNode() {
                return testplugin.Activator.PLUGIN_ID;
            }
        };
    }

    @Override
    protected String getContentType() {
        return EditorSourceConfiguration.CONTENT_TYPE_DOT;
    }

    @Override
    protected String getFileExtension() {
        return "templ";
    }

    @Test
    public void testHTMLTagClose() throws Exception {
        String source = "<html";
        List<IProblem> errors = getParseErrors(source, IHTMLConstants.HTML_PROBLEM);
        assertThat((Collection<IProblem>) errors, is(not(empty() ) ) );
    }

    @Test
    public void testDotSemicolonWarning() throws Exception {
        String source = "<% it.data %>";
        List<IProblem> errors = getdoTParseErrors(source);
        IProblem problem = errors.get(0);
        assertProblem(problem, "Missing semicolon", 1, Severity.WARNING.intValue(), 3);
    }

    @Test
    public void testDotForSyntaxError() throws Exception {
        String source = "<%~ it.data : a : a %>";
        List<IProblem> errors = getdoTParseErrors(source);

        assertProblem(errors.get(0), "Wrong syntax", 1, Severity.ERROR.intValue(), 0);
    }

    @Test
    public void testDotFor() throws Exception {
        String source = "<%~ it.data : a %>";
        List<IProblem> errors = getParseErrors(source, IJSConstants.JS_PROBLEM_MARKER_TYPE);
        assertProblem(errors.get(0), "Unclosed block tag", 1, Severity.ERROR.intValue(), 0);
    }

    @Test
    public void testDotIfEmpty() throws Exception {
        String source = "<%? %>";
        List<IProblem> problems = getdoTParseErrors(source);
        
        assertProblem(problems.get(0), "Add here something", 1, Severity.ERROR.intValue(), 0);
    }

    @Test
    public void testDotElse() throws Exception {
        String source = "<%?sas?%>";
        List<IProblem> errors = getdoTParseErrors(source);

        assertProblem(errors.get(0),
                "Else tag ends itself and you can't add anything bedween that", 1,
                Severity.ERROR.intValue(), 0);
    }
    
    @Test
    public void testGlobalItInDefaultTag() throws Exception {
        String source = "<% it.data; %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertNoErrors(errors);
    }
    
    @Test
    public void testGlobalDefInDefaultTag() throws Exception {
        String source = "<% def.var; %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertProblem(errors.get(0), "def is not allowed in this tag", 1, Severity.ERROR.intValue(), 3);
    }
    
    @Test
    public void testGlobalItInForTag() throws Exception {
        String source = "<%~ it.data : data %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertProblem(errors.get(0), "Unclosed block tag", 1, Severity.ERROR.intValue(), 0);
    }
    
    @Test
    public void testGlobalDefInForTag() throws Exception {
        String source = "<%~ def.var : data %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertProblem(errors.get(0), "def is not allowed in this tag", 1, Severity.ERROR.intValue(), 4);
    }
    
    @Test
    public void testGlobalDefInRuntimeTag() throws Exception {
        String source = "<%# def.var; %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertNoErrors(errors);
    }
    
    @Test
    public void testGlobalItInRuntimeTag() throws Exception {
        String source = "<%# it.data; %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertProblem(errors.get(0), "it is not allowed in this tag", 1, Severity.ERROR.intValue(), 4);
    }
    
    @Test
    public void testTestDotTagEnd() throws Exception {
        String source = "<% <% %>";
        List<IProblem> errors = getdoTParseErrors(source);
        assertProblem(errors.get(0), "Missing doT.js end tag", 1, Severity.ERROR.intValue(), source.length() - 1);
    }
    
    private static void assertNoErrors(List errors) {
        assertThat((Collection<IProblem>) errors, is(empty() ) );
    }
    
    private List<IProblem> getdoTParseErrors(String source) throws Exception {
        return getParseErrors(source, IJSConstants.JS_PROBLEM_MARKER_TYPE);
    }
    
    private List<IProblem> getParseErrors(String source, String lang) throws Exception {
        return getParseErrors(source, (IParseState) new HTMLParseState(source), lang);
    }
}