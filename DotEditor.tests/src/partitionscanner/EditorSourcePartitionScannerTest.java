package partitionscanner;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import javax.swing.text.html.HTML;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.aptana.editor.html.HTMLSourceConfiguration;

import editor.EditorPartitionScanner;

/**
 * @see com.aptana.editor.html.HTMLSourcePartitionScannerTest
 * @author Mihkel
 */
public class EditorSourcePartitionScannerTest {

    private IDocumentPartitioner _partitioner;
    
    @After
    public void tearDown() throws Exception {
        _partitioner = null;
    }

    @Test
    public void testDotDefaultParition() {
        String source = "<% uuu %>";
        
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 0);
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 1);
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 2);
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 3);
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 4);
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 5);
        assertContentType(EditorPartitionScanner.DOT_TAG, source, 6);
    }
    
    @Test
    public void testDotRuntimePartition() throws Exception {
        String source = "<%# uuu %>";
        
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 0);
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 1);
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 2);
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 3);
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 4);
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 5);
        assertContentType(EditorPartitionScanner.DOT_RUNTIME_TAG, source, 6);
    }
    
    @Test
    public void testDotIfPartition() throws Exception {
        String source = "<%? uuu %>";
        
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 0);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 1);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 2);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 3);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 4);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 5);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 6);
    }
    
    @Test
    public void testDotElsePartition() throws Exception {
        String source = "<%??%>";
        
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 0);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 1);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 2);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 3);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 4);
        assertContentType(EditorPartitionScanner.DOT_IF_TAG, source, 5);
    }
    
    private void assertContentType(String contentType, 
                                   String source, int offset) {
        assertEquals("Content type doesn't match expectations at " + offset + " for: " + source.charAt(offset), contentType,
                getContentType(source, offset));
    }

    private String getContentType(String source, int offset) {
        if (_partitioner == null) {
            IDocument doc = new Document(source);
            DotTestUtil.attachPartitioner(doc);
            _partitioner = doc.getDocumentPartitioner();
        }
        return _partitioner.getContentType(offset);
    }
}