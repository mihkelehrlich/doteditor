package partitionscanner;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;

import com.aptana.editor.common.ExtendedFastPartitioner;
import com.aptana.editor.common.IExtendedPartitioner;
import com.aptana.editor.common.NullPartitionerSwitchStrategy;
import com.aptana.editor.common.text.rules.CompositePartitionScanner;
import com.aptana.editor.common.text.rules.NullSubPartitionScanner;
import com.aptana.editor.html.HTMLSourceConfiguration;

import editor.EditorSourceConfiguration;

public abstract class DotTestUtil {

    public static void attachPartitioner(IDocument doc) {
        CompositePartitionScanner partitionScanner = new CompositePartitionScanner(
                        EditorSourceConfiguration.getDefault().createSubPartitionScanner(),
                        new NullSubPartitionScanner(), new NullPartitionerSwitchStrategy() );
        
        IDocumentPartitioner partitioner = new ExtendedFastPartitioner(partitionScanner,
                EditorSourceConfiguration.getDefault().getContentTypes() );
        partitionScanner.setPartitioner((IExtendedPartitioner) partitioner);
        partitioner.connect(doc);
        doc.setDocumentPartitioner(partitioner);
    }
}
