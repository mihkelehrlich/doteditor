package contentassist;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.texteditor.ITextEditor;
import org.hamcrest.core.StringContains;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import partitionscanner.DotTestUtil;
import testplugin.Activator;

import com.aptana.core.tests.TestProject;
import com.aptana.core.util.CollectionsUtil;
import com.aptana.core.util.StringUtil;
import com.aptana.editor.common.AbstractThemeableEditor;
import com.aptana.editor.common.tests.util.AssertUtil;
import com.aptana.editor.epl.tests.EditorTestHelper;
import com.aptana.editor.html.HTMLTestUtil;

import dot.DotAssistProcessor;
import dot.DotGlobals;
import dot.DotTags;

public class DotContentAssistProcessorTest extends DotEditorBasedTests {
    
    private static TestProject _project;
    private DotAssistProcessor _processor;
    private ITextEditor _editor;
    private IFile _file;

    private static final String DOT_EDITOR_ID = "editor.Editor";
    
    
    @BeforeClass
    public static void setUpTests() throws Exception {
        _project = createTestProject();
    }

    @AfterClass
    public static void tearDownTests() throws CoreException {
        _project.delete();
    }
    
    @Before
    public void tearDown() throws Exception {
        _processor = null;
    }
    
    @Test
    public void testDotTagMiddle() throws Exception {
        _file = _project.createFile("test1.templ", "<% %>");

        _editor = makeNewEditor(_file);
        _processor = makeNewAssistProcessor(_editor);
        ITextViewer textViewer = ((AbstractThemeableEditor) _editor).getISourceViewer();
        ICompletionProposal[] proposals = 
                _processor.doComputeCompletionProposals(textViewer, 3, '\0', false);
        
        assertThat(proposals[0].getDisplayString(), containsString(DotGlobals.it.toString() ) );
    }
    
    
    @Test
    public void testDotTagBeginning() throws Exception {
        _file = _project.createFile("test2.templ", "<% %>");
        _editor = makeNewEditor(_file);
        _processor = makeNewAssistProcessor(_editor);
        ITextViewer textViewer = ((AbstractThemeableEditor) _editor).getISourceViewer();
        
        ICompletionProposal[] proposals = 
                _processor.doComputeCompletionProposals(textViewer, 2, '\0', false);
        
        assertProposalsArray(proposals, 
                             DotTags.DOT_IF_TAG.getPrefex(), 
                             DotTags.DOT_ELSE_TAG.getPrefex(),
                             DotTags.DOT_FOR_TAG.getPrefex(),
                             DotTags.DOT_RUNTIME_TAG.getPrefex() );
    }
    
    @Test
    public void testDotFor() throws Exception {
        _file = _project.createFile("test3.templ", "<%~  %>");
        _editor = makeNewEditor(_file);
        _processor = makeNewAssistProcessor(_editor, DotTags.DOT_FOR_TAG);
        ITextViewer textViewer = ((AbstractThemeableEditor) _editor).getISourceViewer();
        
        ICompletionProposal[] proposals = 
                _processor.doComputeCompletionProposals(textViewer, 4, '\0', false);
        
        assertThat(proposals[0].getDisplayString(), containsString(DotGlobals.it.toString() ) );
    }
    
    @Test
    public void testDotIf() throws Exception {
        _file = _project.createFile("test4.templ", "<%?  %>");
        _editor = makeNewEditor(_file);
        _processor = makeNewAssistProcessor(_editor, DotTags.DOT_IF_TAG);
        ITextViewer textViewer = ((AbstractThemeableEditor) _editor).getISourceViewer();
        
        ICompletionProposal[] proposals = 
                _processor.doComputeCompletionProposals(textViewer, 4, '\0', false);
        
        assertThat(proposals[0].getDisplayString(), containsString(DotGlobals.it.toString() ) );
    }
    
    @Test
    public void testDotRuntime() throws Exception {
        _file = _project.createFile("test5.templ", "<%#  %>");
        _editor = makeNewEditor(_file);
        _processor = makeNewAssistProcessor(_editor, DotTags.DOT_RUNTIME_TAG);
        ITextViewer textViewer = ((AbstractThemeableEditor) _editor).getISourceViewer();
        
        ICompletionProposal[] proposals = 
                _processor.doComputeCompletionProposals(textViewer, 4, '\0', false);
        
        assertThat(proposals[0].getDisplayString(), containsString(DotGlobals.def.toString() ) );
    }
    
    
    /**
     * Method for asserting proposals list.
     * Uses assertThat. Runs over the expected array to validate
     * proposals array. <b>Order must be exact</b>.
     * For runs to expected array end.
     * @param proposals
     * @param expected
     */
    private void assertProposalsArray(ICompletionProposal[] proposals, String... expected) {
        for (int i = 0; i < expected.length; i++) {
            assertThat(proposals[i].getDisplayString(), containsString(expected[i]) );
        }
    }

    private static TestProject createTestProject() throws CoreException {
        return new TestProject("Proposals project", new String[] { "com.aptana.projects.webnature" });
    }
    
    private ITextEditor makeNewEditor(IFile file) throws PartInitException {
        return (ITextEditor) EditorTestHelper.openInEditor(file, DOT_EDITOR_ID, true);
    }
    
    private DotAssistProcessor makeNewAssistProcessor(ITextEditor editor) {
        return new DotAssistProcessor((AbstractThemeableEditor) _editor);
    }
    
    private DotAssistProcessor makeNewAssistProcessor(ITextEditor editor, DotTags tag) {
        return new DotAssistProcessor((AbstractThemeableEditor) _editor, tag);
    }
}
