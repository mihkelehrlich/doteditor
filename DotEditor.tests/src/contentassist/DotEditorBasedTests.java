package contentassist;
import org.osgi.framework.Bundle;


import com.aptana.editor.common.AbstractThemeableEditor;
import com.aptana.editor.common.CommonContentAssistProcessor;
import com.aptana.editor.common.EditorContentAssistBasedTests;

import dot.DotAssistProcessor;
import testplugin.Activator;

public class DotEditorBasedTests extends EditorContentAssistBasedTests<DotAssistProcessor> {

    @Override
    protected DotAssistProcessor createContentAssistProcessor(
            AbstractThemeableEditor editor) {
        return new DotAssistProcessor(editor);
    }

    @Override
    protected Bundle getBundle() {
        return Activator.getDefault().getBundle();
    }

    @Override
    protected String getEditorId() {
        return Activator.PLUGIN_ID;
    }
}