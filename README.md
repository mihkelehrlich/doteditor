### Redaktoris on kasutatud [Aptana](https://github.com/aptana/studio3) pistiku lähtekoodi. ###

# Juhend loodud redaktori käivitamiseks arenduskeskkonnas Eclipse #

Redaktori käivitamiseks arenduskeskkonnas on vaja:

* Eclipse IDE (soovituslikult vähemalt versiooni 4.3, sest sellega on proovitud);

* Aptana lähtekoodi, mille saab Aptana repositooriumist aadressilt:
https://github.com/aptana/studio3.

Seejärel tuleb importida projektid Eclipse arenduskeskkonda. Redaktor on projektis DotEditor ja testid DotEditor.tests. Kui arendustööriistale pole paigaldatud Aptana pistikut siis peaks importima kõik projektid mis on Aptana plugin kaustas. Aptana pistiku saab nende koduleheküljelt (vt http://www.aptana.com/products/studio3/success_plugin.html). Juhul kui Aptana pistik on paigaldatud siis peab importima vaid *com.aptana.editor.html, com.aptana.editor.js, com.aptana.editor.svg, com.aptana.editor.xml, com.aptana.build.ui*. Testide projekti vajab projekte veel lisaks projekte* com.aptana.editor.common, com.aptana.scripting, com.aptana.projects, com.aptana.testing.mocks, com.aptana.buildpath.core*. Projektides on klassid mida doT.js redaktor kasutab.

Redaktori käivitamiseks tuleb paremklõpsata projektil ning valida Run As -> Eclipse Application. Kui redaktoril puudub koodivalideerimine siis tuleb see sätetest sisse lülitada. Selleks tuleb avanenud Eclipse arendustööriistas võtta lahti sätted (Preferences) ning otsida „*validation*“. Leitud Aptana Studio all ongi *Validation*. Sealt valikust tuleb sisse lülitada *Dot Syntax Validation* ning märkida valik *Reconcile*.
Testide jaoks tuleb paremklõpsata ühel testil või testide kogumikul (testsuite) ning valida *Run As -> JUnit Plug-in Test*. Tähele tuleb panna, et testide käivitumine võtab kaua aega.