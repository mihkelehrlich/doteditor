package dot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Enum for doT global variables. See constructor for more info.
 * 
 * @see #DotGlobals(String, String[], String[])
 * @author Mihkel
 */
public enum DotGlobals {
    it ("Class with many objects", 
        new DotTags[] { DotTags.DOT_START_TAG, DotTags.DOT_IF_TAG, DotTags.DOT_FOR_TAG },
        new String[] { "data", "util", "esc" }),
    def ("Dot runtime class",
         new DotTags[] { DotTags.DOT_RUNTIME_TAG },
         new String[] { "set", "get", "t" });

    private String _globalDesc;
    private DotTags[] _globalTags;
    private String[] _globalOthers;

    public String getGlobalDesc() {
        return _globalDesc;
    }

    public DotTags[] getGlobalTags() {
        return _globalTags;
    }

    public String[] getGlobalOthers() {
        return _globalOthers;
    }

    /**
     * Constructor for declaring doT global variables. Private because we need
     * to define global variables only here.
     * 
     * @param desc
     *            What that global is for
     * @param tags
     *            In witch tags is it allowed to use
     * @param others
     *            What other objects or functions that object has
     */
    private DotGlobals(String desc, DotTags[] tags, String[] others) {
        _globalDesc = desc;
        _globalOthers = others;
        _globalTags = tags;
    }

    /**
     * Method that filters out globals based on filter string. Uses
     * <code>Enum.values</code> to get all enum values. Filters by tags, if
     * match then we can expect it in return value.
     * 
     * @param tagFilter Filter 
     * @return DotGlobals array, based on tagFilter
     */
    public static DotGlobals[] getDotGlobals(DotTags tagFilter) {
        Stream<DotGlobals> stream = Arrays.stream(DotGlobals.values() )
                .filter(global -> Arrays.asList(global._globalTags).contains(
                        tagFilter) );
        return stream.toArray(size -> new DotGlobals[size]);
    }
    
    public static List<String> getAllOthers() {
        List<String> others = new ArrayList<String>();
        for (DotGlobals global : DotGlobals.values() ) {
            Arrays.asList(global._globalOthers)
                .forEach(other -> others.add(other) );
        }
        return others;
    }

}
