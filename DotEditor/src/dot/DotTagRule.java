package dot;

import java.util.List;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;

public class DotTagRule extends MultiLineRule {
	
	private String _startTag;
	private String _endTag;
	
	private int _unreadCount = 0;

	/**
	 * Constructor what calls private constructor where is defined end tag as constant.
	 * In Dot.js end tag doesn't change so much, so it isn't parameter.
	 * @param startTag
	 * @param token
	 */
	public DotTagRule(DotTags startTag, IToken token) {
		this(startTag.getTag(), DotTags.DOT_END_TAG.getTag(), token);
	}
	
	private DotTagRule(String startTag, String endTag, IToken token) {
		super(startTag, endTag, token);
		_startTag = startTag;
		_endTag = endTag;
	}

	
	/**
	 * Method for finding out is it doT tag.
	 * Chances are that tag is not <code>DotTags.DOT_START_TAG</code>
	 * 
	 */
	@Override
	@SuppressWarnings("unused")
	protected boolean sequenceDetected(ICharacterScanner scanner,
									   char[] sequenceArray,
									   boolean eofAllowed) {
		int c = scannerRead(scanner);
		if (isRightTag(_startTag, sequenceArray) ) {
			if (isDefaultTag(scanner) ) {
				if (!isSpecialTag(scanner) ) {
					scannerUnread(scanner);
					return false;
				}
			}
			scannerUnread(scanner);
		} else if (isRightTag(_endTag, sequenceArray) ) {
			scannerUnread(scanner);
		}
		return super.sequenceDetected(scanner, sequenceArray, eofAllowed);
	}
	

	/**
	 * Checks if it is  doT tag (if, else...)
	 * @return
	 */
	private boolean isSpecialTag(ICharacterScanner scanner) {
		List<DotTags> tags = DotTags.getSpecialTags();
		int c = scannerRead(scanner);
		String tagString = null;
		char ca;
		
		for (DotTags tag : tags) {
			tagString = tag.getTag();
			ca = tryCharAt(tagString, 2);    
			    if (_startTag.equals(tagString) && c == ca)
			        return true;
		}
		return false;
	}
	/**
	 * Like String.charAt returns character on parameter location and when
	 *  no character is on that location new exception is thrown.
	 * When it is uncertain (sometimes returns with exception) then it is good to use this
	 *  method (like with doT tags, start tag is shorter than special tag).
	 * @param str String where to take that character
	 * @param index Position where to take that character
	 * @see String#charAt(int)
	 * @return Character/ char on given position, when charAt returns exception then
	 *  space <code>(' ')</code> is returned.
	 */
	private char tryCharAt(String str, int index) {
        try {
            return str.charAt(index);  
        } catch (IndexOutOfBoundsException e) {
            return ' ';            
        }
    } 
	
	/**
	 * Checks if it is default tag what we are dealing with.
	 * @param scanner
	 * @return True when it is so.
	 */
	private boolean isDefaultTag(ICharacterScanner scanner) {
		int c = scannerRead(scanner);
		if (c != ' ') {
			scannerUnreadOnce(scanner);
			return true;
		} else
			return false;
		
	}
	
	/**
	 * Checks if this is tag what we are expecting
	 * @param correctTag Tag what we expect
	 * @param charSequence Actual tag in char array
	 * @return if it is right tag
	 */
	private boolean isRightTag(String correctTag, char[] charSequence) {
		int tagLenght = correctTag.length();
		for (int i = 0; i < tagLenght; i++) {
			if (charSequence[i] != correctTag.charAt(i) )
				return false;
		}
		return true;
	}
	
	/**
	 * Method for read and count times so it is easier to unread.
	 * Scanner unreads are important for super method.
	 * Scanner has its cursor like reader.
	 * Read method moves cursor and reads element. Unread moves cursor back.
	 * @param scanner
	 * @return
	 */
	private int scannerRead(ICharacterScanner scanner) {
		_unreadCount++;
		return scanner.read();
	}
	
	/**
	 * Method for correct unread. 
	 * Scanner unreads are important for super method.
	 * Scanner has its cursor like reader.
	 * Read method moves cursor and reads element. Unread moves cursor back
	 * @param scanner scanner to be unread
	 * @param unreadCount times to unread
	 */
	private void scannerUnread(ICharacterScanner scanner) {
		for (int i = 1; i <= _unreadCount; i++) {
			scanner.unread();
		}
		_unreadCount = 0;
	}
	
	/**
	 * Use only if you want to unread once not when you "know" that there was one read.
	 * Method for correct unread. Unreads once, like scanner.unread does, but this also subtracts
	 * one from counter to keep counter correct.
	 * Scanner unreads are important for super method.
	 * Scanner has its cursor like reader.
	 * Read method moves cursor and reads element. Unread moves cursor back
	 * @param scanner scanner to be unread
	 * @param unreadCount times to unread
	 */
	private void scannerUnreadOnce(ICharacterScanner scanner) {
		scanner.unread();
		_unreadCount--;
	}
}
