package dot;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Stack;

import beaver.Scanner.Exception;
import beaver.Symbol;

import com.aptana.core.build.IProblem.Severity;
import com.aptana.editor.html.parsing.HTMLParseState;
import com.aptana.editor.html.parsing.HTMLParser;
import com.aptana.js.core.IJSConstants;
import com.aptana.parsing.IParseState;
import com.aptana.parsing.ParseResult;
import com.aptana.parsing.ParseState;
import com.aptana.parsing.ParserPoolFactory;
import com.aptana.parsing.WorkingParseResult;
import com.aptana.parsing.ast.IParseError;
import com.aptana.parsing.ast.IParseNode;
import com.aptana.parsing.ast.ParseError;
import com.aptana.parsing.ast.ParseRootNode;

import editor.EditorSourceConfiguration;

/**
 * Dot specific parser for error marking Use it as any other parser
 * 
 * @author Mihkel
 */
public class DotParser extends HTMLParser {

    private DotParserScanner _scanner;
    private WorkingParseResult _workingParseResult;
    private HTMLParseState _parseState;
    private ParseRootNode _currentElem;
    private Symbol _dotSymbol;
    private Symbol _symbol;
    private String _tag;
    private LinkedList<Integer> _unclosedElems;

    /**
     * In this method we override IParseState and WorkingParseResult objects for
     * super method. Because we need those methods in doT parsing side.
     * 
     * @see DotParser#subparse(String, Symbol, String)
     */
    @Override
    protected void parse(IParseState parseState, WorkingParseResult working)
            throws java.lang.Exception {
        _workingParseResult = working;
        String source = parseState.getSource();
        if (parseState instanceof HTMLParseState) {
            _parseState = (HTMLParseState) parseState;
        } else {
            _parseState = new HTMLParseState(source, parseState.getStartingOffset(),
                    parseState.getSkippedRanges());
            _parseState.setProgressMonitor(parseState.getProgressMonitor());
        }
        int startingOffset = _parseState.getStartingOffset();
        ParseRootNode rootNode = new DotParseRootNode(startingOffset, startingOffset
                + source.length() - 1);
        _currentElem = rootNode;
        _unclosedElems = new LinkedList<Integer>();
        try {
            super.parse(_parseState, _workingParseResult);
        } finally {
            // even when parseResult and state
            // are nulled we still nullify them
            if (_workingParseResult != null && !_unclosedElems.isEmpty() ) {
                for (Integer entry : _unclosedElems ) {
                    addNewError(IJSConstants.CONTENT_TYPE_JS, entry.intValue(), 1, "Unclosed block tag", Severity.ERROR);
                }
            }
            _workingParseResult = null;
            _parseState = null;
            _currentElem = null;
            _scanner = null;
        }
    }

    /**
     * Uses {@link DotParserScanner} to get new token. <b>Scanner must be
     * initialized</b>
     * 
     * @see DotParser#setupDotParserScanner(String)
     * @return Symbol from DotScanner
     * @throws IOException
     * @throws Exception
     */
    private Symbol nextToken() throws IOException, Exception {
        return _scanner.nextToken();
    }

/**
     * When symbol is "<" then we can be sure that
     * it isn't HTML tag. It means HTMLParserScanner did't handle
     * that symbol. It is possible that we have doT tag here.
     * 
     * @see DotParser#processAsDot(Symbol, String)
     */
    @Override
    protected void processSymbol(Symbol symbol, String source) throws IOException,
            Exception {

        if (symbol.value.toString().equals("<")) {
            processAsDot(symbol, source);
        } else {
            super.processSymbol(symbol, source);
        }
    }

    /**
     * Method where we process if it is doT tag. If it is we subparse doT tag
     * contents.
     * 
     * @param symbol
     *            Used for DotParserScanner source length
     * @param source
     *            Used for DotParserScanner
     * @throws IOException
     * @throws Exception
     */
    private void processAsDot(Symbol symbol, String source) throws IOException, Exception {
        String scannerSource = source.substring(symbol.getStart());
        setupDotParserScanner(scannerSource);
        _symbol = symbol;
        if (parseAsDot()) {
            if (!proccessAsSpecialTag()) {
                String text = getTextInsideDot(_dotSymbol);
                validateDotGlobals(text);
                subparse(IJSConstants.CONTENT_TYPE_JS, symbol, text);
            } else {
                parseAsSpecialTag();
            }
        } else {
            super.processSymbol(symbol, source);
        }
    }

    private void parseAsSpecialTag() {
        if (_tag.equals(DotTags.DOT_IF_TAG.getTag())) {
            parseDotIfOrElse();
        } else if (_tag.equals(DotTags.DOT_FOR_TAG.getTag())) {
            parseDotFor();
        }
    }

    private void parseDotFor() {
        final String forFormatTmpl = "for (%2$s in %1$s ) {}";

        String text = getTextInsideDot(_dotSymbol);
        validateDotGlobals(text);
        String[] forArgs = text.split(":");
        if (forArgs.length == 2) {
            String jsFor = String.format(forFormatTmpl, forArgs[0].trim(),
                    forArgs[1].trim());
            subparse(IJSConstants.CONTENT_TYPE_JS, _symbol, jsFor);
            markAsUnclosed();
        } else if (checkForBlockEndTag(_dotSymbol.value.toString() ) ) {
            markAsClosed();
        } else {
            addNewError(IJSConstants.CONTENT_TYPE_JS, _symbol.getStart(),
                    _dotSymbol.getStart() + _dotSymbol.getEnd(), "Wrong syntax",
                    Severity.ERROR);
        }
    }
    
    private boolean checkForBlockEndTag(String tag) {
        final String blockEndTag = DotTags.DOT_FOR_TAG.getTag() + DotTags.DOT_END_TAG.getTag();
            
        return tag.equals(blockEndTag);
    }

    private void parseDotIfOrElse() {
        String dotText = _dotSymbol.value.toString();
        String endTag = dotText.substring(dotText.length() - 3, dotText.length());
        boolean isIf = endTag.trim().equals(DotTags.DOT_END_TAG.getTag());
        if (isIf) {
            validateDotGlobals(getTextInsideDot(_dotSymbol) );
            parseDotIf();
            markAsUnclosed();
        } else if (checkIfBlockEndTag(dotText) ) {
            markAsClosed();
        } else {
            checkDotElse(dotText);
        }
    }
    
    private boolean checkIfBlockEndTag(String tag) {
        final String blockEndTag = DotTags.DOT_IF_TAG.getTag() + DotTags.DOT_END_TAG.getTag();
        final int blockTagLength = blockEndTag.length();
        String endTag = tag.substring(tag.length() - 3, tag.length() );
        
        return (endTag.startsWith(DotTags.DOT_IF_TAG.getPrefex() ) &&
                tag.length() == blockTagLength);
    }
    
    private void parseDotIf() {
        String text = getTextInsideDot(_dotSymbol);
        if (text.trim().length() != 0) {
            String jsIf = "if (" + text + ") {}";
            subparse(IJSConstants.CONTENT_TYPE_JS, _symbol, jsIf);
        } else {
            addNewError(IJSConstants.CONTENT_TYPE_JS, _symbol.getStart(),
                    _dotSymbol.getStart() + _dotSymbol.getEnd(),
                    "Add here something", Severity.ERROR);
        }
    }
    
    private void checkDotElse(String tag) {
        if (!tag.equals(DotTags.DOT_ELSE_TAG.getTag())) {
            addNewError(IJSConstants.CONTENT_TYPE_JS, _symbol.getStart(),
                    _dotSymbol.getStart() + _dotSymbol.getEnd(),
                    "Else tag ends itself and you can't add anything bedween that",
                    Severity.ERROR);
        }
    }
    
    private void markAsUnclosed() {
        _unclosedElems.addFirst(_symbol.getStart());
    }
    
    private void markAsClosed() {
        _unclosedElems.removeLast();
    }

    /**
     * Special tags aren't {@link DotTags#DOT_START_TAG},
     * {@link DotTags#DOT_RUNTIME_TAG}
     * 
     * @return
     */
    private boolean proccessAsSpecialTag() {
        _tag = _dotSymbol.value.toString().substring(0, 3);
        if (_tag.endsWith(" ") || _tag.endsWith("#"))
            return false;
        return true;
    }

    /**
     * Uses {@link DotParserScanner} to get new token. Also finds if there is
     * any unclosed doT lines.
     * 
     * @see DotParser#nextToken()
     * @return boolean true, if it is doT.
     * @throws IOException
     * @throws Exception
     */
    private boolean parseAsDot() throws IOException, Exception {
        _dotSymbol = nextToken();
        boolean isDotTag = _dotSymbol.value.toString().startsWith(
                DotTags.DOT_START_TAG.getTag() );
        if (isDotTag)
            if (!checkIfDotIsClosed())
                addNewError(IJSConstants.CONTENT_TYPE_JS, _dotSymbol.getEnd(), 1,
                        "Missing doT.js end tag", Severity.ERROR);

        return isDotTag;
    }
    
    private boolean checkIfDotIsClosed() {
        String source = _dotSymbol.value.toString();

        int startTagsCount = countOccurrences(source, DotTags.DOT_START_TAG.getTag());
        int endTagsCount = countOccurrences(source, DotTags.DOT_END_TAG.getTag());;

        return startTagsCount == endTagsCount;
    }
    
    /**
     * Method for finding string/ word
     * occurrences in string.<br>
     * Uses String.indexOf. If index of returns -1 then there isn't
     * any of that string.
     * 
     * @param from Source string where to search
     * @param find String what to look for
     * @return
     * 
     * @see {@link http://stackoverflow.com/questions/767759/occurrences-of-substring-in-a-string}
     */
    private int countOccurrences(String from, String find) {
        int count = 0, index = 0;
        
        while (index != -1) {
            index = from.indexOf(find, index);
            
            if (index != -1) {
                count++;
                index += find.length();
            }
        }
        
        return count;
    }

    /**
     * Initializes DotParserScanner if needed
     * 
     * @param scannerSource
     */
    private void setupDotParserScanner(String scannerSource) {
        if (_scanner == null) {
            _scanner = new DotParserScanner();
        }
        _scanner.setSource(scannerSource);
    }

    private void validateDotGlobals(String source) {
        DotGlobals[] dotGlobals; 
        if (_tag.equals(DotTags.DOT_RUNTIME_TAG.getTag() ) ) {
            dotGlobals = DotGlobals.getDotGlobals(DotTags.DOT_START_TAG);
        } else {
            dotGlobals = DotGlobals.getDotGlobals(DotTags.DOT_RUNTIME_TAG);
        }
        
        for (DotGlobals global : dotGlobals) {
            if (source.contains(global.toString() ) ) {
                int offset = source.indexOf(global.toString() ) + 3;
                
                addNewError(IJSConstants.CONTENT_TYPE_JS, _symbol.getStart() + offset, global.toString().length(),
                        global.toString() + " is not allowed in this tag", Severity.ERROR);
            }
        }
    }
    
    /**
     * Subparses text.<br>
     * Uses {@link ParserPoolFactory#parse(String, IParseState) }
     * 
     * Also adds _parseState skipped ranges. These are for HTMLParser, to skip
     * those lines.
     * 
     * @param lang
     *            Parseable lang.
     * @param symbol
     *            For ParseState offset
     * @param text
     *            Parseable string
     */
    private void subparse(String lang, Symbol symbol, String text) {
        try {
            ParseState subParseState = new ParseState(text, symbol.getStart() );
            ParseResult parseResult = ParserPoolFactory.parse(lang, subParseState);

            IParseNode skipNode = makeSkipNode(symbol.getStart(), text.length() );
            _parseState.setSkippedRanges(new IParseNode[] { skipNode });
            for (IParseError error : parseResult.getErrors()) {
                addNewError(lang, symbol, error);
            }

            if (_currentElem != null) {
                IParseNode parseNode = parseResult.getRootNode();
                _currentElem.addChild(parseNode);
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * Method for making node for skipped ranges
     * @param startOffset starting position
     * @param textLenght  symbol.value().toString.length(). Use {@link DotParser#_dotSymbol}
     * @return
     */
    private IParseNode makeSkipNode(int startOffset, int textLenght) {
        // + 5 cause we substring doT tags out.
        return new ParseRootNode(new Symbol[]{}, startOffset, textLenght + 5) {

            @Override
            public String getLanguage() {
                return EditorSourceConfiguration.CONTENT_TYPE_DOT;
            }};
    }

    private String getTextInsideDot(Symbol symbol) {
        String text = (String) symbol.value;
        return text.substring(3, text.length() - 2);
    }

    /**
     * Adds new error to workingParseResult
     * 
     * @param lang
     *            What language problem it is
     * @param symbol
     *            Used of offsets
     * @param error
     *            Used for message and severity
     */
    private void addNewError(String lang, Symbol symbol, IParseError error) {
        addNewError(lang, symbol.getStart() + error.getOffset() + 3, error.getLength(),
                error.getMessage(), error.getSeverity());
    }

    /**
     * Adds new error to workingParseResult
     * 
     * @param lang
     *            What language problem it is
     * @param startOffset
     *            Use original symbol for that
     * @param errorLength
     *            Use symbol from doT scanner for that
     * @param errorMessage
     * @param severity
     */
    private void addNewError(String lang, int startOffset, int errorLength,
            String errorMessage, Severity severity) {
        _workingParseResult.addError(new ParseError(lang, startOffset, errorLength,
                errorMessage, severity));
    }
}
