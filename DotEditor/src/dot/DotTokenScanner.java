package dot;

import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;

import com.aptana.editor.common.parsing.CompositeTokenScanner;
import com.aptana.editor.common.parsing.IScannerSwitchStrategy;
import com.aptana.editor.common.parsing.ScannerSwitchStrategy;
import com.aptana.editor.html.parsing.HTMLScanner;
import com.aptana.editor.html.parsing.lexer.HTMLTokens;

import editor.EditorCodeScanner;

public class DotTokenScanner extends CompositeTokenScanner {

    private static final String[] DOT_ENTER_TOKENS = new String[] {
        dotTokenToString(DotTags.DOT_START_TAG), dotTokenToString(DotTags.DOT_FOR_TAG), 
        dotTokenToString(DotTags.DOT_RUNTIME_TAG), dotTokenToString(DotTags.DOT_IF_TAG) };
    private static final String[] DOT_EXIT_TOKENS = new String[] { dotTokenToString(DotTags.DOT_END_TAG) };
    private static final String[] CSS_ENTER_TOKENS = new String[] { HTMLTokens.getTokenName(HTMLTokens.STYLE) };
    private static final String[] CSS_EXIT_TOKENS = new String[] { HTMLTokens.getTokenName(HTMLTokens.STYLE_END) };
    private static final String[] JS_ENTER_TOKENS = new String[] { HTMLTokens.getTokenName(HTMLTokens.SCRIPT) };
    private static final String[] JS_EXIT_TOKENS = new String[] { HTMLTokens.getTokenName(HTMLTokens.SCRIPT_END) };

    private static final IScannerSwitchStrategy DOT_STRATEGY = new ScannerSwitchStrategy(
            DOT_ENTER_TOKENS, DOT_EXIT_TOKENS);
    private static final IScannerSwitchStrategy CSS_STRATEGY = new ScannerSwitchStrategy(CSS_ENTER_TOKENS,
            CSS_EXIT_TOKENS);
    private static final IScannerSwitchStrategy JS_STRATEGY = new ScannerSwitchStrategy(JS_ENTER_TOKENS, JS_EXIT_TOKENS);

    
    public static final short EOF = HTMLTokens.EOF;
    public static final short DOT_TOKEN = 13;

    public DotTokenScanner() {
        this(new EditorCodeScanner(), new IScannerSwitchStrategy[] { DOT_STRATEGY, CSS_STRATEGY, JS_STRATEGY });
    }

    private DotTokenScanner(ITokenScanner primaryTokenScanner,
            IScannerSwitchStrategy[] switchStrategies) {
        super(primaryTokenScanner, switchStrategies);
    }

    public short getTokenType(Object data) {
        if (getCurrentSwitchStrategy() == DOT_STRATEGY) {
           return getDotShort(data.toString() );
        }
        return new HTMLScanner().getTokenType(data);
    }
    
    private short getDotShort(String tag) {
        if (dotTokenToString(DotTags.DOT_START_TAG).equals(tag) )
            return 13;
        else if (dotTokenToString(DotTags.DOT_IF_TAG).equals(tag) )
            return 14;
        else if (dotTokenToString(DotTags.DOT_RUNTIME_TAG).equals(tag) )
            return 15;
        else if (dotTokenToString(DotTags.DOT_FOR_TAG).equals(tag) )
            return 16;
        else if (dotTokenToString(DotTags.DOT_ELSE_TAG).equals(tag) )
            return 17;
        else
            return -1;
    }
    
    @Override
    public IToken nextToken() {
        if (getCurrentSwitchStrategy() == DOT_STRATEGY){
            reset();
        }
        return super.nextToken();
    }
    
    private static String dotTokenToString(DotTags tag) {
        return tag.getToken().getData().toString();
    }
}
