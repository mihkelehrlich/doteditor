package dot;

import beaver.Symbol;

import com.aptana.parsing.ast.ParseRootNode;

import editor.EditorSourceConfiguration;

public class DotParseRootNode extends ParseRootNode {

    public DotParseRootNode(int start, int end) {
        super(new Symbol[0], start, end);
    }

    @Override
    public String getLanguage() {
        return EditorSourceConfiguration.CONTENT_TYPE_DOT;
    }
}
