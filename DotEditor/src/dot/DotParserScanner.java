package dot;

import org.eclipse.jface.text.rules.IToken;

import beaver.Symbol;

import com.aptana.editor.common.parsing.CompositeParserScanner;
import com.aptana.editor.common.parsing.CompositeTokenScanner;
/**
 * Scanner what is used for parsing
 * 
 * @see DotParser
 * @author Mihkel
 */
public class DotParserScanner extends CompositeParserScanner {

    public DotParserScanner() {
        this(new DotTokenScanner() );
    }
    
    private DotParserScanner(CompositeTokenScanner tokenScanner) {
        super(tokenScanner);
    }
    
    @Override
    protected Symbol createSymbol(int start, int end, String text, IToken token) {
        short type = DotTokenScanner.EOF;
        Object data = token.getData();
        
        if (data != null) {
            type = ((DotTokenScanner) getTokenScanner() ).getTokenType(data);
        }
        
        return new Symbol(type, start, end, text);
    };
}
