package dot;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

/**
 * Enum class for DotTags. Use this class when you need doT tags.
 * Use get dot beginning tags (<code>getTag()</code>), prefixes (<code>getPrefex()</code>)
 *  and descriptions (<code>getDesc()</code>).
 * All tags (tags strings, tags prefixes) are defined in {@link TagValues}.
 * No setter methods here.
 * @author Mihkel
 * @see #DotTags(String, String, String)
 */
public enum DotTags {
    
    DOT_START_TAG (TagValues.DOT_START_TAG, "", "Dot default tag", new Token("DOT_START_TAG") ),
    DOT_IF_TAG (TagValues.IF_TAG, TagValues.IF_PREFIX, "Dot if tag", new Token("DOT_IF_TAG") ),
    /**
     * This tag is special.
     * This tag ends itself.
     * This tag is one-line and can't add anything between it.
     */
    DOT_ELSE_TAG (TagValues.ELSE_TAG, TagValues.ELSE_PREFIX, "Dot else tag", new Token("DOT_IF_TAG") ),
    DOT_FOR_TAG (TagValues.FOR_TAG, TagValues.FOR_PREFIX, "Dot for-each", new Token("DOT_FOR_TAG") ),
    DOT_RUNTIME_TAG (TagValues.RUNTIME_TAG, TagValues.RUNTIME_PREFIX, "Dot runtime tag", new Token("DOT_RUNTIME_TAG") ),
    DOT_END_TAG ("%>", "", "Dot end tag", new Token("DOT_END_TAG") );
    
    private String _tag;
    private String _prefex;
    private String _desc;
    private IToken _token;
    
    public IToken getToken() {
        return _token;
    }

    public String getDesc() {
        return _desc;
    }

    public String getTag() {
        return _tag;
    }

    public String getPrefex() {
        return _prefex;
    }
    /**
     * Constructor for defining doT tags.
     * It is private because we don't need to define
     *  outside DotTags class file.
     * @param tag Tag string. Comes from {@link TagValues} class.
     * @param prefex Tag prefix string. Comes from {@link TagValues} class.
     * @param desc Tag description string.
     */
    private DotTags(String tag, String prefex, String desc, IToken token) {
        _tag = tag;
        _prefex = prefex;
        _desc = desc;
        _token = token;
    }
    
    /**
     * Only list of special tags (if, else, for, runtime tag).
     * Start and end tag removed.
     */
    public static LinkedList<DotTags> getSpecialTags() {
       LinkedList<DotTags> specials = new LinkedList<DotTags>(Arrays.asList(DotTags.values() ) );
       specials.remove(DotTags.DOT_START_TAG);
       specials.remove(DotTags.DOT_END_TAG);
       return specials;
    }
    
    public static final HashMap<String, String> TAGS_HASHMAP;
    static {
        TAGS_HASHMAP = new HashMap<String, String>();
        Arrays.asList(DotTags.values() )
            .forEach(tag -> TAGS_HASHMAP.put(tag.getPrefex(), tag.getDesc() ) );
    }
    
    /**
     * All tag values (tag strings, tag prefixes) are defined here.
     * <br>
     * This class is made because of enum class limitations.
     * In enum class firstly you need to define enum values. Then other values.
     * We can't use any properties in enum values because in compile time those haven't compiled yet.
     * @author Mihkel
     */
    class TagValues {
        public static final String DOT_START_TAG = "<%";
        public static final String DOT_END_TAG = "%>";
        
        public static final String IF_PREFIX = "?"; // DotTagRule expects one mark long string
        public static final String ELSE_PREFIX = IF_PREFIX + "?"; // DotTagRule expects two mark long string
        public static final String FOR_PREFIX = "~";
        public static final String RUNTIME_PREFIX = "#";
        
        public static final String IF_TAG = DOT_START_TAG + IF_PREFIX;
        /** This tag ends itself! */
        public static final String ELSE_TAG = DOT_START_TAG + ELSE_PREFIX + DOT_END_TAG;
        public static final String RUNTIME_TAG = DOT_START_TAG + RUNTIME_PREFIX;
        public static final String FOR_TAG = DOT_START_TAG + FOR_PREFIX;
    }
}
