package dot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ContextInformation;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;

import com.aptana.editor.common.AbstractThemeableEditor;
import com.aptana.editor.common.CommonContentAssistProcessor;
import com.aptana.editor.common.contentassist.CommonCompletionProposal;
import com.aptana.editor.js.contentassist.JSContentAssistProcessor;

/**
 * Class for doT code assists.
 * 
 * @author Mihkel
 *
 */
public class DotAssistProcessor extends JSContentAssistProcessor {
    
    /**
     * When this constuctor called then _tag will be {@link DotTags#DOT_START_TAG}.
     * That means all proposals will be like in usual doT
     * 
     * @param editor Needed for CommonContentAssistProcessor
     * 
     * @see DotTags
     * @see CommonContentAssistProcessor
     */
    public DotAssistProcessor(AbstractThemeableEditor editor) {
        super(editor);
        _tag = DotTags.DOT_START_TAG;
    }

    protected DotTags _tag;
    private DotGlobals _globalVar;
    private boolean _tailingDot;

    /**
     * Constructor where we show what tag/ code we are dealing with. Tag is used
     * for giving correct proposals
     * 
     * @param editor
     * @param tag
     *            Dot beginning tag
     */
    public DotAssistProcessor(AbstractThemeableEditor editor, DotTags tag) {
        super(editor);
        _tag = tag;
    }

    /**
     * {@link IContentAssistProcessor#computeCompletionProposals(ITextViewer, int) }
     */
    @Override
    public ICompletionProposal[] doComputeCompletionProposals(
            ITextViewer textViewer, int offset, char activationChar,
            boolean autoActivated) {
        
        List<ICompletionProposal> proposalsList = new ArrayList<ICompletionProposal>();
        
        String fromDoc = getStringFromDocument(textViewer.getDocument(), offset);
        
        
        String fromDocTail = "";
        String fromDocCursorFront = "";
        if (fromDoc != "") {
            int length = fromDoc.length();
            fromDocTail = Character.toString(fromDoc.charAt(length - 1) );
            fromDocCursorFront = Character.toString(fromDoc.charAt(length - 2) );
        }
            
        if (checkIfRightAfterTag(fromDocCursorFront)) {
            if (_tag == DotTags.DOT_START_TAG) {
                proposalsList.addAll(proposeDotTagsPrefexes(offset));
            }
            offset++;
        } else if (fromDocTail.equals(".") ) {
            _tailingDot = true;
        }

        if (checkIfGlobal(fromDoc, offset)) {
            proposalsList.addAll(getGlobals(_globalVar, offset));
        } else {
            proposalsList.addAll(getGlobals(_tag, offset));
        }

        ICompletionProposal[] jsProposals = super.doComputeCompletionProposals(textViewer, offset, activationChar, autoActivated);
        proposalsList.addAll(Arrays.asList(jsProposals) );
        
        return proposalsList.toArray(new ICompletionProposal[proposalsList
                .size()]);
    }
    
    /**
     * To override proposals sorting and other proposals what we
     * don't need at this moment.
     * It just returns {@link #doComputeCompletionProposals}.
     * 
     */
    public ICompletionProposal[] computeCompletionProposals(ITextViewer textViewer,
            int offset, char activationChar, boolean autoActivated) {
        return doComputeCompletionProposals(textViewer, offset, activationChar, autoActivated);
    }

    /**
     * Tries to get string what is three characters long from cursor position
     * 
     * @param doc
     *            Document where that string comes from.
     * @param offset
     *            Cursor position
     * @return String from document, three characters long (because longest doT
     *         tag is three characters long). If IDocument.get()  gets exception
     *         then empty string will be returned.
     */
    private String getStringFromDocument(IDocument doc, int offset) {
        String fromDoc = "";
        try { 
            if (offset < 4)
                fromDoc = doc.get(0, offset) + " "; 
            else
                fromDoc = doc.get(offset - 4, 5);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        return fromDoc;
    }

    /**
     * Checks if input string is end of the doT tag and matches doT suffix. Uses
     * <code>DotTags.values</code>. Use in if statements.
     * 
     * @param possibleTag
     *            Suffix of the tag
     * @return true if is one of the inputs matches, false when none
     */
    private boolean checkIfRightAfterTag(String possibleTag) {
        for (DotTags tag : DotTags.values()) {
            if (tag.getTag().endsWith(possibleTag)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if input contains global variable. Returns true, if it contains
     * global variable and also values <code>_globalVar</code>
     * 
     * @param textViewer
     * @param offset
     * @return true when contains global value, false when not.
     */
    private boolean checkIfGlobal(String fromDoc, int offset) {
        for (DotGlobals global : DotGlobals.values()) {
            if (fromDoc.contains(global.toString())) {
                _globalVar = global;
                return true;
            }
        }
        return false;
    }

    /**
     * Makes array of doT tag prefixes. Where are tags and description for each
     * tag. Uses {@link #DotTags.getSpecialTags()} to get special tags.
     * 
     * @param offset
     * @return List of completion proposals.
     */
    private List<ICompletionProposal> proposeDotTagsPrefexes(int offset) {
        List<DotTags> tags = DotTags.getSpecialTags();
        List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
        
        String displayString;
        for (DotTags tag : tags) {
            displayString = makeDisplayString(tag.getPrefex(),
                    tag.getDesc());
            proposals.add(new CommonCompletionProposal(tag.getPrefex(), offset, 
                    tag.getPrefex().length() - 1, // for else
                    tag.getPrefex().length(), null, displayString,
                    new ContextInformation(displayString, displayString),
                    displayString));
        }

        return proposals;
    }

    /**
     * Method for getting global variables. Filters by input/ by tag. Returns
     * globals that are possible in that tag. Uses
     * <code>DotGlobals.getDotGlobals</code>.
     * 
     * @param filterTag
     *            DotTag. Usually from IDocument. Used to get context, what
     *            variables to propose
     * @param offset
     *            Offset position. Used for position, where to make auto
     *            complete.
     * @return List of globals, {@link ICompletionProposal}, where are replace
     *         string, (dotGlobals,toString() ), {@link makeDisplayString}
     *         (dotGlobals.toString(), dotGlobals.getGlobalDesc()) ,offset
     *         position, ContextInformation(displayString, dotGlobal
     *         desciption).
     */
    private List<ICompletionProposal> getGlobals(DotTags filterTag, int offset) {
        DotGlobals[] globals = DotGlobals.getDotGlobals(filterTag);
        List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();

        String propTail = "";
        if(!_tailingDot) {
            propTail = ".";
        }
        
        String dispString;
        for (DotGlobals dotGlobals : globals) {
            dispString = makeDisplayString(dotGlobals.toString(),
                    dotGlobals.getGlobalDesc());
            proposals.add( // TODO: same thing proposeDotTagsPrefexes
                    new CommonCompletionProposal(dotGlobals.toString() + propTail, offset, 0,
                            dotGlobals.toString().length() + propTail.length(), null, dispString,
                            new ContextInformation(dispString, dispString),
                            dotGlobals.getGlobalDesc()));
        }
        return proposals;
    }

    /**
     * Method for getting global variables. Uses DotGlobal to global variables
     * (methods other children objects). Returns that global variable variables
     * and methods.
     * 
     * @param globalVar
     *            Variable that we are dealing with right now.
     * @param offset
     *            Cursor position.
     * @return List of globals, where are global variable variable/ method as
     *         replacement string, position, replacement string.
     */
    private List<ICompletionProposal> getGlobals(DotGlobals globalVar,
            int offset) {
        List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();

        for (String other : globalVar.getGlobalOthers() ) {
            proposals.add(new CommonCompletionProposal(other, offset, 0, other
                    .length(), null, other, null, "") );
        }
        return proposals;
    }

    /**
     * Method for making display string in CompletionProposal. Adds proposable
     * string, separator and description
     * 
     * @param replacementString
     *            Proposable string
     * @param desc
     *            Propose description
     * @return Concated string with propose and description. TODO: HTML
     */
    private String makeDisplayString(String replacementString, String desc) {
        return replacementString + " : " + desc;
    }

    /**
     * If typing a method call with several parameters use this to show the
     * applicable parameter types. The current parameter where the cursor is
     * will be shown in bold.
     * http://www.rossenstoyanchev.org/write/prog/eclipse/eclipse3.html
     */
    @Override
    public IContextInformation[] computeContextInformation(
            ITextViewer textViewer, int offset) {
        IContextInformation[] info = new IContextInformation[1];
        info[0] = new ContextInformation("Test contextDispString",
                "Test infoString");
        return info;
    }
}
