package dot;

import helpers.TextAttributesProvider;
import helpers.WhitespaceDetector;
import helpers.WordDetector;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;

import com.aptana.editor.common.text.rules.ExtendedToken;

/**
 * Scans inside the doT tags and does syntax
 * highlight 
 * @author Mihkel
 */
public class DotTagScanner extends RuleBasedScanner {
	
	public DotTagScanner() {
		IToken stringToken = new ExtendedToken(TextAttributesProvider.DOT_STRING);
		IToken commentToken = new ExtendedToken(TextAttributesProvider.DOT_COMMENT);
		IToken dotGlobal = new ExtendedToken(TextAttributesProvider.DOT_GLOBAL);
		
		List<IRule> rules = new ArrayList<IRule>();

		rules.add(new SingleLineRule("\"", "\"", stringToken) );
		rules.add(new SingleLineRule("'", "'", stringToken) );
             
		rules.add(new SingleLineRule("//", "", commentToken) );
		rules.add(new MultiLineRule("/*", "*/", commentToken) );
		
		rules.add(new WhitespaceRule(new WhitespaceDetector() ) );
		
		// Globals
		for (DotGlobals global : DotGlobals.values() ) {
            rules.add(new WordRule(new WordDetector(global.toString() ), dotGlobal) );

            for (String other : global.getGlobalOthers() ) {
                rules.add(new WordRule(new WordDetector(other), dotGlobal) );
            }		
		}
		 
		setRules(rules.toArray(new IRule[rules.size()]) );
	}
}