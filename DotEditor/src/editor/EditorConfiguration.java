package editor;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.source.ISourceViewer;

import com.aptana.core.util.StringUtil;
import com.aptana.editor.common.AbstractThemeableEditor;
import com.aptana.editor.common.CompositeSourceViewerConfiguration;
import com.aptana.editor.common.IPartitionerSwitchStrategy;
import com.aptana.editor.html.HTMLSourceConfiguration;

public class EditorConfiguration extends CompositeSourceViewerConfiguration {
    
    public EditorConfiguration(IPreferenceStore preferences, AbstractThemeableEditor editor) {
        super(HTMLSourceConfiguration.getDefault(),EditorSourceConfiguration.getDefault(),
              preferences, editor);
    }

    @Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler reconciler = (PresentationReconciler) super.getPresentationReconciler(sourceViewer);
		EditorSourceConfiguration.getDefault().setupPresentationReconciler(reconciler, sourceViewer);
		return reconciler;
	}
    
    @Override
    protected IContentAssistProcessor getContentAssistProcessor(
            ISourceViewer sourceViewer, String contentType) {
        if (getEditor() == null) {
            System.out.println("Editor == null");
            return null;
        }
        
        return EditorSourceConfiguration.getDefault().getContentAssistProcessor(getEditor(), contentType);
    }

    @Override
    public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
        IContentAssistant assistant = super.getContentAssistant(sourceViewer);
        return assistant;
    }

    @Override
    protected String getTopContentType() {
        return EditorSourceConfiguration.CONTENT_TYPE_DOT;
    }

    @Override
    protected IPartitionerSwitchStrategy getPartitionerSwitchStrategy() {
        return EditorPartitionerSwitchStrategy.getDefault();
    }

    @Override
    protected String getStartEndTokenType() {
        return StringUtil.EMPTY;
    }
}
