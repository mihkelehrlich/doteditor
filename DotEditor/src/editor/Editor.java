package editor;

import com.aptana.editor.html.HTMLEditor;

public class Editor extends HTMLEditor {
    
    public Editor() {
        super();
        setSourceViewerConfiguration(new EditorConfiguration(getPreferenceStore(), this) );
        setDocumentProvider(new EditorDocumentProvider() );
    }
    
    @Override
    public void dispose() {
        super.dispose();
    }
        
    @Override
    public String getContentType() {
        return EditorSourceConfiguration.CONTENT_TYPE_DOT;
    }
}