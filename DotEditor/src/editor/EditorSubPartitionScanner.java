package editor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

import com.aptana.editor.common.IPartitionScannerSwitchStrategy;
import com.aptana.editor.common.PartitionScannerSwitchStrategy;
import com.aptana.editor.common.text.rules.CompositeSubPartitionScanner;
import com.aptana.editor.common.text.rules.ExtendedToken;
import com.aptana.editor.common.text.rules.ISubPartitionScanner;
import com.aptana.editor.common.text.rules.SubPartitionScanner;
import com.aptana.editor.html.HTMLSourceConfiguration;
import com.aptana.editor.html.parsing.HTMLUtils;
import com.aptana.editor.js.JSSourceConfiguration;
import com.aptana.editor.svg.SVGSourceConfiguration;

public class EditorSubPartitionScanner extends CompositeSubPartitionScanner {
   
   private static final int TYPE_JS = 1;
   private static final int TYPE_HTML = TYPE_DEFAULT;
    
   private static final String[] JS_SWITCH_SEQUENCES = new String[] { "</script>" }; //$NON-NLS-1$
    
   public EditorSubPartitionScanner() {
       super(new ISubPartitionScanner[] {
               new SubPartitionScanner(
                       EditorSourceConfiguration.getDefault().getPartitioningRules(),
                       EditorSourceConfiguration.CONTENT_TYPES, new Token(HTMLSourceConfiguration.DEFAULT) ),
               JSSourceConfiguration.getDefault().createSubPartitionScanner()
             },
             new IPartitionScannerSwitchStrategy[] {
               new PartitionScannerSwitchStrategy(JS_SWITCH_SEQUENCES)
             }
       );
   }
   
   @Override
   public void setLastToken(IToken token) {
        super.setLastToken(token);
        if (token == null) return;
        
        Object data = token.getData();
        
        if (!(data instanceof String)) {
            current = TYPE_DEFAULT;
            return;
        }
        
        String contentType = (String) data;
        
        if (contentType.equals(HTMLSourceConfiguration.HTML_SCRIPT) 
                || contentType.equals(SVGSourceConfiguration.SCRIPT) ) {
            if (!(token instanceof ExtendedToken && 
                    ((HTMLUtils.isTagSelfClosing(((ExtendedToken) token).getContents())) || !HTMLUtils
                    .isJavaScriptTag(((ExtendedToken) token).getContents())))) {
                current = TYPE_JS;
                super.setLastToken(null);
            }
        } else if (contentType.equals(HTMLSourceConfiguration.DEFAULT) || 
                    contentType.equals(IDocument.DEFAULT_CONTENT_TYPE) ) {
            current = TYPE_HTML;
//                super.setLastToken(null);
        } else if (contentType.startsWith(EditorPartitionScanner.DOT) ) {
            current = TYPE_DEFAULT;
        }
        else {
            for (int i = 0; i < subPartitionScanners.length; ++i)
            {
                if (subPartitionScanners[i].hasContentType(contentType))
                {
                    current = i;
                    break;
                }
            }
        }
   }
}
