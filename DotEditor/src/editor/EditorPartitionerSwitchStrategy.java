package editor;

import com.aptana.editor.common.PartitionerSwitchStrategy;

import dot.DotTags;

public class EditorPartitionerSwitchStrategy extends PartitionerSwitchStrategy {

    private static EditorPartitionerSwitchStrategy instance;
    
    private static final String[][] DOT_TAGS = new String[][] {
        {DotTags.DOT_START_TAG.getTag(), DotTags.DOT_END_TAG.getTag()},
        {DotTags.DOT_FOR_TAG.getTag(), DotTags.DOT_END_TAG.getTag()},
        {DotTags.DOT_RUNTIME_TAG.getTag(), DotTags.DOT_END_TAG.getTag()},
        {DotTags.DOT_IF_TAG.getTag(), DotTags.DOT_END_TAG.getTag()}
    };
    
    public static EditorPartitionerSwitchStrategy getDefault() {
        if (instance == null)
            instance = new EditorPartitionerSwitchStrategy();
        return instance;
    }
    
    private EditorPartitionerSwitchStrategy() {
        super(DOT_TAGS);
    }
    
    @Override
    public String[][] getSwitchTagPairs() {
        return DOT_TAGS;
    }

}
