package editor;

import helpers.TextAttributesProvider;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;

import com.aptana.editor.common.AbstractThemeableEditor;
import com.aptana.editor.common.CommonEditorPlugin;
import com.aptana.editor.common.IPartitioningConfiguration;
import com.aptana.editor.common.ISourceViewerConfiguration;
import com.aptana.editor.common.TextUtils;
import com.aptana.editor.common.scripting.IContentTypeTranslator;
import com.aptana.editor.common.scripting.QualifiedContentType;
import com.aptana.editor.common.text.rules.CaseInsensitiveMultiLineRule;
import com.aptana.editor.common.text.rules.CompositePartitionScanner;
import com.aptana.editor.common.text.rules.ExtendedToken;
import com.aptana.editor.common.text.rules.FixedMultiLineRule;
import com.aptana.editor.common.text.rules.ISubPartitionScanner;
import com.aptana.editor.common.text.rules.PartitionerSwitchingIgnoreRule;
import com.aptana.editor.common.text.rules.TagRule;
import com.aptana.editor.html.HTMLSourceConfiguration;
import com.aptana.editor.html.core.IHTMLConstants;
import com.aptana.js.core.IJSConstants;

import dot.DotAssistProcessor;
import dot.DotTagRule;
import dot.DotTagScanner;
import dot.DotTags;

public class EditorSourceConfiguration implements IPartitioningConfiguration, ISourceViewerConfiguration {

    private DotTagScanner _dotTagScanner;
    private DotTagScanner _dotIfScanner;
    private DotTagScanner _dotRuntimeScanner;
    private DotTagScanner _dotForScanner;

    public static final String CONTENT_TYPE_DOT = "editor.dotType";
    public static final String CONTENT_TYPE_HTML_DOT = "editor.dotType.html";
    
    public static final String[] CONTENT_TYPES = new String[] { EditorPartitionScanner.DOT,
        EditorPartitionScanner.DOT_TAG, EditorPartitionScanner.DOT_IF_TAG, 
        EditorPartitionScanner.DOT_ELSE_TAG, EditorPartitionScanner.DOT_RUNTIME_TAG,
        EditorPartitionScanner.DOT_FOR_TAG};
    
    public static final String[][] TOP_CONTENT_TYPE = new String[][] { {CONTENT_TYPE_DOT} };
    

    private EditorSourceConfiguration() {}

    
    @Override
    public String[][] getTopContentTypes() {
        return TOP_CONTENT_TYPE;
    }

    @Override
    public void setupPresentationReconciler(PresentationReconciler reconciler,
            ISourceViewer sourceViewer) {
        
        DefaultDamagerRepairer damagerRepairer = new DefaultDamagerRepairer(getDotTagScanner() );
        reconciler.setDamager(damagerRepairer, EditorPartitionScanner.DOT_TAG);
        reconciler.setRepairer(damagerRepairer, EditorPartitionScanner.DOT_TAG);

        damagerRepairer = new DefaultDamagerRepairer(getDotIfTagScanner() );
        reconciler.setDamager(damagerRepairer, EditorPartitionScanner.DOT_IF_TAG);
        reconciler.setRepairer(damagerRepairer, EditorPartitionScanner.DOT_IF_TAG);

        damagerRepairer = new DefaultDamagerRepairer(getDotForTagScanner() );
        reconciler.setDamager(damagerRepairer, EditorPartitionScanner.DOT_FOR_TAG);
        reconciler.setRepairer(damagerRepairer, EditorPartitionScanner.DOT_FOR_TAG);
        
        damagerRepairer = new DefaultDamagerRepairer(getDotRuntimeTagScanner() );
        reconciler.setDamager(damagerRepairer, EditorPartitionScanner.DOT_RUNTIME_TAG);
        reconciler.setRepairer(damagerRepairer, EditorPartitionScanner.DOT_RUNTIME_TAG);
    }
    
    private static EditorSourceConfiguration instance;
    
    static {
        IContentTypeTranslator translator = CommonEditorPlugin.getDefault().getContentTypeTranslator();
        
        // Top-level
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT), 
                new QualifiedContentType("text.html.basic") );
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT, IDocument.DEFAULT_CONTENT_TYPE), 
                new QualifiedContentType("text.html.basic") );
        
        // Inside doT.js tags
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT, CONTENT_TYPE_DOT), 
                new QualifiedContentType("text.html.basic", "source.dot.embedded.block.html") );
        
        // Outside doT.js tags
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT, IHTMLConstants.CONTENT_TYPE_HTML), 
                new QualifiedContentType("text.html.basic") );
        
        // JS
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT, IJSConstants.CONTENT_TYPE_JS), 
                new QualifiedContentType("text.html.basic", "source.js.embedded.html") );
        
        // Dot start tag
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT, CompositePartitionScanner.START_SWITCH_TAG), 
                new QualifiedContentType("text.html.basic", "source.dot.embedded.block.html", "punctuation.section.embedded.begin.dot") );
        
        // Dot end tag
        translator.addTranslation(
                new QualifiedContentType(CONTENT_TYPE_HTML_DOT, CompositePartitionScanner.END_SWITCH_TAG), 
                new QualifiedContentType("text.html.basic", "source.dot.embedded.block.html", "punctuation.section.embedded.end.dot") );
        
    }
        
    public static EditorSourceConfiguration getDefault() {
        if (instance == null) 
            return new EditorSourceConfiguration();
        return instance;
    }

    @Override
    public IContentAssistProcessor getContentAssistProcessor(AbstractThemeableEditor editor,
                    String contentType) {
        if (contentType.equals(EditorPartitionScanner.DOT_TAG) ) {
            return new DotAssistProcessor(editor, DotTags.DOT_START_TAG);
        } else if (contentType.equals(EditorPartitionScanner.DOT_IF_TAG) ) {
            return new DotAssistProcessor(editor, DotTags.DOT_IF_TAG);
        } else if (contentType.equals(EditorPartitionScanner.DOT_FOR_TAG) ){
            return new DotAssistProcessor(editor, DotTags.DOT_FOR_TAG);
        } else if (contentType.equals(EditorPartitionScanner.DOT_RUNTIME_TAG) ) {
            return new DotAssistProcessor(editor, DotTags.DOT_RUNTIME_TAG);
        } else if (contentType.startsWith(EditorPartitionScanner.DOT) ) {
            return new DotAssistProcessor(editor, DotTags.DOT_START_TAG);
        } else
            return HTMLSourceConfiguration.getDefault().getContentAssistProcessor(editor, contentType);
    }

    @Override
    public IPredicateRule[] getPartitioningRules() {
        IToken dotTag = new ExtendedToken(EditorPartitionScanner.DOT_TAG);
        IToken dotIf = new ExtendedToken(EditorPartitionScanner.DOT_IF_TAG);
        IToken dotFor = new ExtendedToken(EditorPartitionScanner.DOT_FOR_TAG);
        IToken dotRuntime = new ExtendedToken(EditorPartitionScanner.DOT_RUNTIME_TAG);

        return new IPredicateRule[] {
            new DotTagRule(DotTags.DOT_START_TAG, dotTag),
            new DotTagRule(DotTags.DOT_IF_TAG, dotIf),
            new DotTagRule(DotTags.DOT_RUNTIME_TAG, dotRuntime),
            new DotTagRule(DotTags.DOT_FOR_TAG, dotFor),
            new TagRule("style", new ExtendedToken(new Token(HTMLSourceConfiguration.HTML_STYLE)), true), //$NON-NLS-1$
            new TagRule("script", new ExtendedToken(new Token(HTMLSourceConfiguration.HTML_SCRIPT)), true), //$NON-NLS-1$
            new TagRule("/", new Token(HTMLSourceConfiguration.HTML_TAG_CLOSE)), //$NON-NLS-1$
            new TagRule(new ExtendedToken(new Token(HTMLSourceConfiguration.HTML_TAG))),
            new CaseInsensitiveMultiLineRule("<!DOCTYPE ", ">",new Token(HTMLSourceConfiguration.HTML_DOCTYPE)), //$NON-NLS-1$ //$NON-NLS-2$
            new PartitionerSwitchingIgnoreRule(new FixedMultiLineRule("<!--", "-->", new Token(HTMLSourceConfiguration.HTML_COMMENT), (char) 0, true)) //$NON-NLS-1$ //$NON-NLS-2$
        };
    }

    @Override
    public ISubPartitionScanner createSubPartitionScanner() {
        return new EditorSubPartitionScanner();
    }

    @Override
    public String getDocumentContentType(String contentType) {
        if (contentType.startsWith(CONTENT_TYPE_DOT) ) return CONTENT_TYPE_DOT;
        return HTMLSourceConfiguration.getDefault().getDocumentContentType(contentType);
    }
    
    public String[] getContentTypes() {
        return TextUtils.combine(new String[][] {CONTENT_TYPES, 
                HTMLSourceConfiguration.getDefault().getContentTypes() });
    }

    private DotTagScanner getDotTagScanner() {
        if (_dotTagScanner == null) {
            _dotTagScanner = new DotTagScanner();
            _dotTagScanner.setDefaultReturnToken(
                    new ExtendedToken(TextAttributesProvider.DOT_DEFAULT) );
        }
        return _dotTagScanner;
    }

    /**
     * @return Makes new DotTagScanner with if tag color
     */
    private DotTagScanner getDotIfTagScanner() {
        if (_dotIfScanner == null) {
            _dotIfScanner = new DotTagScanner();
            _dotIfScanner.setDefaultReturnToken(
                    new ExtendedToken(TextAttributesProvider.DOT_CONDITIONAL) );
        }
        return _dotIfScanner;
    }

    private DotTagScanner getDotForTagScanner() {
        if (_dotForScanner == null) {
            _dotForScanner = new DotTagScanner();
            _dotForScanner.setDefaultReturnToken(
                    new ExtendedToken(TextAttributesProvider.DOT_FOREACH) );
        }
        return _dotForScanner;
    }

    private DotTagScanner getDotRuntimeTagScanner() {
        if (_dotRuntimeScanner == null) {
            _dotRuntimeScanner = new DotTagScanner();
            _dotRuntimeScanner.setDefaultReturnToken(
                    new ExtendedToken(TextAttributesProvider.DOT_RUNTIME) );
        }
        return _dotRuntimeScanner;
    }
}
