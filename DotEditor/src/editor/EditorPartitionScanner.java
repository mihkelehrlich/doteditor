package editor;

import helpers.WordDetector;

import org.eclipse.jface.text.rules.*;

import dot.DotTagRule;
import dot.DotTags;

public class EditorPartitionScanner extends RuleBasedPartitionScanner {
    public final static String DOT = "__dot";
	public final static String DOT_TAG = "__dot_tag";
	public final static String DOT_IF_TAG = "__dot_conditional_tag";
	public final static String DOT_ELSE_TAG = "__dot_else_tag";
	public final static String DOT_RUNTIME_TAG = "__dot_runtime_tag";
	public final static String DOT_FOR_TAG = "__dot_for_tag";
	public final static String DOT_END_TAG = "__dot_end_tag";
	
	public EditorPartitionScanner() {
		IToken dotTag = new Token(DOT_TAG);
		IToken dotIf = new Token(DOT_IF_TAG);
		IToken dotElse = new Token(DOT_ELSE_TAG);
		IToken dotFor = new Token(DOT_FOR_TAG);
		IToken dotRuntime = new Token(DOT_RUNTIME_TAG);
		
		IPredicateRule[] rules = new IPredicateRule[5];
		rules[0] = new DotTagRule(DotTags.DOT_START_TAG, dotTag);
		rules[1] = new DotTagRule(DotTags.DOT_IF_TAG, dotIf);
		rules[2] = new DotTagRule(DotTags.DOT_RUNTIME_TAG, dotRuntime);
		rules[3] = new DotTagRule(DotTags.DOT_FOR_TAG, dotFor);
		rules[4] = new WordPrdicateRule(new WordDetector(DotTags.DOT_ELSE_TAG.getTag() ), dotElse); 
		
		setPredicateRules(rules);
	}
	
	/**
	 * Idea from <a href="http://svn.codehaus.org/groovy/trunk/groovy/ide/groovy-eclipse/org.codehaus.groovy.eclipse.ui/src/org/codehaus/groovy/eclipse/editor/GroovyPartitionScanner.java">
	 * 		http://svn.codehaus.org/groovy/trunk/groovy/ide/groovy-eclipse/org.codehaus.groovy.eclipse.ui/src/org/codehaus/groovy/eclipse/editor/GroovyPartitionScanner.java</a>
	 * @author Mihkel
	 */
	static class WordPrdicateRule extends WordRule implements IPredicateRule {
		
		private final IToken _successToken;
		
		public WordPrdicateRule(IWordDetector detector, IToken defaultToken) {
			super(detector, defaultToken);
			_successToken = defaultToken;
		}

		@Override
		public IToken evaluate(ICharacterScanner scanner, boolean paramBoolean) {
			return super.evaluate(scanner);
		}

		@Override
		public IToken getSuccessToken() {
			return _successToken;
		}
	}
}
