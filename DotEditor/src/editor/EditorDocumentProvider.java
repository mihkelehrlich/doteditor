package editor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;

import com.aptana.editor.common.CommonDocumentProvider;
import com.aptana.editor.common.CommonEditorPlugin;
import com.aptana.editor.common.ExtendedFastPartitioner;
import com.aptana.editor.common.IExtendedPartitioner;
import com.aptana.editor.common.NullPartitionerSwitchStrategy;
import com.aptana.editor.common.text.rules.CompositePartitionScanner;
import com.aptana.editor.common.text.rules.NullSubPartitionScanner;

public class EditorDocumentProvider extends CommonDocumentProvider {

    @Override
    public void connect(Object element) throws CoreException {
        super.connect(element);
        
        IDocument doc = getDocument(element);
        
        if (doc != null) {
            CompositePartitionScanner partScanner = 
                    new CompositePartitionScanner(EditorSourceConfiguration.getDefault().createSubPartitionScanner(), 
                                                  new NullSubPartitionScanner(), new NullPartitionerSwitchStrategy() );
            IDocumentPartitioner partitioner = 
                    new ExtendedFastPartitioner(partScanner, EditorSourceConfiguration.getDefault().getContentTypes() );
            
            partScanner.setPartitioner((IExtendedPartitioner) partitioner);
            partitioner.connect(doc);
            doc.setDocumentPartitioner(partitioner);
            CommonEditorPlugin.getDefault().getDocumentScopeManager()
                .registerConfiguration(doc, EditorSourceConfiguration.getDefault() );
        }
    }
    
    @Override
    protected String getDefaultContentType(String filename) {
        return EditorSourceConfiguration.CONTENT_TYPE_HTML_DOT;
    }
}
