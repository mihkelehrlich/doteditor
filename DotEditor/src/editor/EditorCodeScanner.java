package editor;

import helpers.WhitespaceDetector;
import helpers.WordDetector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;

import com.aptana.editor.html.parsing.HTMLTokenScanner;

import dot.DotTagRule;
import dot.DotTags;
import editor.EditorPartitionScanner.WordPrdicateRule;

/**
 * Scans everywhere
 */

public class EditorCodeScanner extends HTMLTokenScanner {
	
	public final static String DOT_TAG_CONDITIONAL = "__dot_if_else";
	public final static String DOT_TAG_FORTAG = "__dot_foreach";
	public final static String DOT_TAG_HASHTAG = "__dot_hashtag";
	public final static String DOT_TAG_TILDE = "__dot_tilde";
	public final static String DOT_START = "__dot_start";
	
	public EditorCodeScanner() {
		IToken string = new Token("TEXT");
		
		
		List<IRule> rules = new ArrayList<IRule>(Arrays.asList(fRules) );
		
		rules.add(new WhitespaceRule(new WhitespaceDetector() ) );
		rules.add(new SingleLineRule("'", "'", string) );
		rules.add(new DotTagRule(DotTags.DOT_START_TAG, DotTags.DOT_START_TAG.getToken() ) ); // 13
        rules.add(new DotTagRule(DotTags.DOT_IF_TAG, DotTags.DOT_IF_TAG.getToken() ) ); // 14
        rules.add(new DotTagRule(DotTags.DOT_RUNTIME_TAG, DotTags.DOT_RUNTIME_TAG.getToken() ) ); // 15
        rules.add(new DotTagRule(DotTags.DOT_FOR_TAG, DotTags.DOT_FOR_TAG.getToken() ) ); // 16
        rules.add(new WordPrdicateRule(new WordDetector(DotTags.DOT_ELSE_TAG.getTag() ), // 17
                                                         DotTags.DOT_ELSE_TAG.getToken() ) );
        rules.add(new WordPrdicateRule(new WordDetector(DotTags.DOT_IF_TAG.getTag() +
                                                        DotTags.DOT_END_TAG.getTag() ),
                                                    DotTags.DOT_IF_TAG.getToken() ) );
        rules.add(new WordPrdicateRule(new WordDetector(DotTags.DOT_FOR_TAG.getTag() +
                                                        DotTags.DOT_END_TAG.getTag() ),
                                                    DotTags.DOT_IF_TAG.getToken() ) );
        
		setRules( rules.toArray(new IRule[rules.size()]) );
	}
}
