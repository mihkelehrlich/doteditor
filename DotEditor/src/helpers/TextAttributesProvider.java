package helpers;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

/**
 * Class for text styles
 * @author Mihkel
 */
public final class TextAttributesProvider {
	public final static TextAttribute DEFAULT = new TextAttribute(new Color(Display.getCurrent(), 250,0,0) );
	public final static TextAttribute STRING = new TextAttribute(new Color(Display.getCurrent(), 0,0,0) );
	
	public final static TextAttribute DOT_DEFAULT = new TextAttribute(new Color(Display.getCurrent(), TextRGB.DOT_DEFAULT) );
	public final static TextAttribute DOT_CONDITIONAL = new TextAttribute(new Color(Display.getCurrent(), TextRGB.DOT_CONDITIONAL) );
	public final static TextAttribute DOT_FOREACH = new TextAttribute(new Color(Display.getCurrent(),  TextRGB.DOT_FOREACH) );
	public final static TextAttribute DOT_RUNTIME = new TextAttribute(new Color(Display.getCurrent(),  TextRGB.DOT_RUNTIME) );
	
	public final static TextAttribute DOT_GLOBAL = new TextAttribute(new Color(Display.getCurrent(), TextRGB.DOT_DEFAULT), null, SWT.BOLD);
	public final static TextAttribute DOT_COMMENT = new TextAttribute(new Color(Display.getCurrent(), 50,150,50) );
	public final static TextAttribute DOT_STRING = new TextAttribute(new Color(Display.getCurrent(), 150,100,250) );

	/**
	 * Because TextAttribute method doesn't have setter methods we need to use this.
	 * When TextAttribute object need style (bold, italic so on) we need to use this.
	 * Uses constructor TextAttribute where third attribute is style (witch we add). 
	 * Other parameters are taken from TextAttribute object what we are going to pass to this method. 
	 * @param textAttr Where to copy foreground and background.
	 * @param style Style what to add to. Usually from SWT package.
	 * @return TextAttribute object with style.
	 */
	public static TextAttribute setNewTextStyle(TextAttribute textAttr, int style) {
		return new TextAttribute(textAttr.getForeground(), textAttr.getBackground(), style);
	}
	
	/**
	 * Class for colors. Some colors depend from others (for example doT if, else).
	 * @author Mihkel
	 */
	private final static class TextRGB {
		public final static RGB DOT_DEFAULT = new RGB(200,140,50);
		public final static RGB DOT_CONDITIONAL = brithColor(DOT_DEFAULT, -60);
		public final static RGB DOT_FOREACH = brithColor(DOT_DEFAULT, 25);
		public final static RGB DOT_RUNTIME = brithColor(DOT_DEFAULT, -15);
		
		/**
		 * Idea from: http://stackoverflow.com/questions/1618169/java-operations-with-colors-add-subtract-colors-in-a-constant-class
		 */
		private static RGB brithColor(RGB rgb, int amount) {
			int red = Math.max(0, Math.min(255, rgb.red + amount) );
			int green = Math.max(0, Math.min(255, rgb.green + amount) );
			int blue = Math.max(0, Math.min(255, rgb.blue + amount) );
			
			return new RGB(red,green,blue);
		}
	};
}
