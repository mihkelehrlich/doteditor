package helpers;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;

/**
 * Class for single mark rule.
 * Use with factory method.
 * 
 * @author Mihkel
 */
public class MarkRule extends WordRule {

	private MarkRule(IWordDetector detector, IToken defaultToken) {
		super(detector, defaultToken);
	}
	
	/**
	 * Method that makes new MarkRule.
	 * Method adds bold style <code>SWT.BOLD</code> to token (TextAttribute is required) 
	 * and makes new MarkRule object.
	 * @param mark is char to restrict that char is one mark long
	 * @param token with TextAttribute, otherwise gets exception
	 * @return MarkRule object.
	 */
	public static MarkRule markRuleFactory(char mark, IToken token) {
		TextAttribute textAttr = (TextAttribute) token.getData();
		TextAttribute newTextAttr = TextAttributesProvider.setNewTextStyle(textAttr, SWT.BOLD);

		return new MarkRule(new WordDetector(Character.toString(mark) ), new Token(newTextAttr));
	}
}
