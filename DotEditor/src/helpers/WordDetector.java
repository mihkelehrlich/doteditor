package helpers;

import org.eclipse.jface.text.rules.IWordDetector;

/**
 * Class for detecting words and single marks.
 * Used in scanners
 * @author Mihkel
 *
 */
public class WordDetector implements IWordDetector {

    private String _word;
    private int _count;
    
    public WordDetector(String word) {
        _word = word;
    }
    
    @Override
    public boolean isWordStart(char startChar) {
        _count = 1;
        return _word.charAt(0) == startChar;
    }

    @Override
    public boolean isWordPart(char inputChar) {
        try {
            if(_word.charAt(_count) == inputChar) {
                _count++;
                return true;
            } else
                return false;
        } catch (StringIndexOutOfBoundsException e){
            return false;
        }
    }
}
