package helpers;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

public class WhitespaceDetector implements IWhitespaceDetector {

	@Override
	public boolean isWhitespace(char character) {
		return (character == ' ' ||
				character == '\t' ||
				character == '\n' ||
			    character == '\r');
	}

}
